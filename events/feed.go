package events

type UserFeedEventType int16

const (
	UserFeedEventTypeUnknown UserFeedEventType = 0
	UserFeedEventUpdated     UserFeedEventType = 1
)

type UserFeedEvent struct {
	UserID    int64             `json:"user_id"`
	EventType UserFeedEventType `json:"type"`
}

func NewUserFeedUpdatedEvent(userID int64) *UserFeedEvent {
	return &UserFeedEvent{UserID: userID, EventType: UserFeedEventUpdated}
}
