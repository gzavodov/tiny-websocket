module gitlab.com/gzavodov/tiny-websocket

go 1.18

require (
	github.com/gorilla/websocket v1.5.0
	github.com/rabbitmq/amqp091-go v1.4.0
)
