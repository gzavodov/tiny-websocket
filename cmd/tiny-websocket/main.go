package main

import (
	"fmt"
	amqp "github.com/rabbitmq/amqp091-go"
	"gitlab.com/gzavodov/tiny-websocket/pkg/ws"
	"log"
)

func main() {
	amqpConn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	failOnError(err, "could not connect to RabbitMQ")

	defer amqpConn.Close()

	userFeedNotificationCh, err := amqpConn.Channel()
	failOnError(err, "could not open a RabbitMQ Channel")

	userFeedNotificationQueue, err := userFeedNotificationCh.QueueDeclare(
		"tiny-social-net-user-feed",
		false,
		false,
		false,
		false,
		nil,
	)
	failOnError(err, "could not declare a RabbitMQ Queue")

	server := ws.NewServer(messageHandler, userFeedNotificationCh, userFeedNotificationQueue.Name)

	log.Fatal(server.Run())
}

func messageHandler(message []byte) {
	fmt.Println(string(message))
}

func failOnError(err error, msg string) {
	if err == nil {
		return
	}

	log.Panicf("%s: %s", msg, err)
}
