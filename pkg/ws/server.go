package ws

import (
	"fmt"
	"github.com/gorilla/websocket"
	amqp "github.com/rabbitmq/amqp091-go"
	"log"
	"net/http"
	"strconv"
)

var (
	connectionUpgrader = websocket.Upgrader{
		CheckOrigin: func(r *http.Request) bool {
			return true // Пропускаем любой запрос
		},
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
	}
)

type Server struct {
	userConnections       map[int64]*websocket.Conn
	messageHandler        func(message []byte)
	notificationChan      *amqp.Channel
	notificationQueueName string
}

func NewServer(messageHandler func(message []byte), notificationChan *amqp.Channel, notificationQueueName string) *Server {
	return &Server{
		userConnections:       make(map[int64]*websocket.Conn),
		messageHandler:        messageHandler,
		notificationChan:      notificationChan,
		notificationQueueName: notificationQueueName,
	}
}

func (s *Server) Run() error {
	log.Print("server is started")
	http.HandleFunc("/echo", s.echo)
	return http.ListenAndServe(":8000", nil)
}

func (s *Server) echo(w http.ResponseWriter, r *http.Request) {
	var userIDParam = r.URL.Query().Get("user_id")
	if len(userIDParam) == 0 {
		http.Error(
			w,
			"parameter user_id must be specified",
			http.StatusBadRequest,
		)
		return
	}

	userID, err := strconv.ParseInt(userIDParam, 10, 64)
	if err != nil {
		http.Error(
			w,
			fmt.Sprintf("failed to parse user_id parameter (%s): %v", userIDParam, err),
			http.StatusBadRequest,
		)
		return
	}

	connection, _ := connectionUpgrader.Upgrade(w, r, nil)
	defer connection.Close()

	s.userConnections[userID] = connection // Сохраняем соединение

	for {
		msgType, msg, err := connection.ReadMessage()

		if err != nil || msgType == websocket.CloseMessage {
			break // Выходим из цикла, если клиент пытается закрыть соединение или связь с клиентом прервана
		}

		go s.messageHandler(msg)
	}
}

func (s *Server) WriteMessage(msg []byte) {
	for _, connection := range s.userConnections {
		connection.WriteMessage(websocket.TextMessage, msg)
	}
}
